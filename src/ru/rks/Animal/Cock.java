package ru.rks.Animal;

/**
 * Класс представляет объект Петух
 */
public class Cock extends Animal {

    protected Cock(String name, String voice) {
        super(name, voice);
    }
    public Cock(){
        this("Петух","Ко-ко-ко");
    }
}
