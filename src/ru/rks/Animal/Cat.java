package ru.rks.Animal;

/**
 * Класс для представления объекта Кот.
 */
public class Cat extends  Animal{

    protected Cat(String name, String voice) {
        super(name, voice);
    }
    public Cat(){
        this("Кошка" ,"Мяу");
    }
}
