package ru.rks.Animal;

/**
 * Класс для представления объекта Собака
 */
public class Dog extends Animal {

    protected Dog(String name, String voice) {
        super(name, voice);
    }

    public Dog() {
        this("Собака", "гав-гав");
    }
}
